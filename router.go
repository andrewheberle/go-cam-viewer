package main

import (
	"github.com/gorilla/mux"
	"github.com/spf13/viper"
)

func newRouter() *mux.Router {
	r := mux.NewRouter()
	s := r.PathPrefix(viper.GetString("base_path")).Subrouter()
	s.HandleFunc("/{cam}/view.{format}", camHandler)
	s.HandleFunc("/{cam}/{width:[0-9]+}/view.{format}", camHandler)
	s.HandleFunc("/{cam}/{width:[0-9]+}/{height:[0-9]+}/view.{format}", camHandler)
	return r
}
