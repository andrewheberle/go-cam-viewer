package main

const (
	defaultAddress  string = ""
	defaultPort     int    = 8080
	defaultBasePath string = "/cams"
)
