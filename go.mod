module gitlab.com/andrewheberle/go-cam-viewer

go 1.12

require (
	github.com/davidbyttow/govips v0.0.0-20190304175058-d272f04c0fea
	github.com/fsnotify/fsnotify v1.4.7
	github.com/gorilla/mux v1.7.2
	github.com/spf13/jwalterweatherman v1.0.0
	github.com/spf13/viper v1.4.0
	github.com/stretchr/testify v1.3.0 // indirect
	gitlab.com/andrewheberle/routerswapper v1.1.1
	golang.org/x/sys v0.0.0-20190621062556-bf70e4678053 // indirect
	golang.org/x/text v0.3.2 // indirect
)
