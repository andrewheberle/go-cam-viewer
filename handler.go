package main

import (
	"crypto/md5"
	"fmt"
	"net/http"
	"path/filepath"
	"strconv"

	"github.com/davidbyttow/govips/pkg/vips"
	"github.com/gorilla/mux"
	jww "github.com/spf13/jwalterweatherman"
	"github.com/spf13/viper"
)

func camHandler(w http.ResponseWriter, r *http.Request) {
	var cam, width, height, format, compression, quality, mtype string
	var imgType vips.ImageType

	vars := mux.Vars(r)

	if _, ok := vars["cam"]; ok {
		cam = vars["cam"]
		jww.INFO.Printf("cam: %s\n", cam)
	}

	if _, ok := vars["width"]; ok {
		width = vars["width"]
		jww.INFO.Printf("width: %s\n", width)
	}

	if _, ok := vars["height"]; ok {
		height = vars["height"]
		jww.INFO.Printf("height: %s\n", height)
	}

	if _, ok := vars["format"]; ok {
		format = vars["format"]
		jww.INFO.Printf("format: %s\n", format)
	}

	if r.FormValue("c") != "" {
		compression = r.FormValue("c")
		jww.INFO.Printf("compression: %s\n", compression)
	}

	if r.FormValue("q") != "" {
		quality = r.FormValue("q")
		jww.INFO.Printf("quality: %s\n", quality)
	}

	// handle format and mime type
	switch format {
	case "jpg", "jpeg":
		mtype = "image/jpeg"
		imgType = vips.ImageTypeJPEG
	case "webp":
		mtype = "image/webp"
		imgType = vips.ImageTypeWEBP
	case "png":
		mtype = "image/png"
		imgType = vips.ImageTypePNG
	case "gif":
		mtype = "image/gif"
		imgType = vips.ImageTypeGIF
	case "svg":
		mtype = "image/svg+xml"
		imgType = vips.ImageTypeSVG
	case "tiff":
		mtype = "image/tiff"
		imgType = vips.ImageTypeTIFF
	default:
		errmsg := fmt.Sprintf("invalid format: %s", format)
		jww.ERROR.Println(errmsg)
		http.Error(w, errmsg, http.StatusBadRequest)
		return
	}

	// check cam is configured
	if !viper.IsSet("cameras." + cam) {
		http.Error(w, "Not Found", http.StatusNotFound)
		return
	}

	thiscam := viper.Sub("cameras." + cam)
	file, err := getLatestImage(thiscam.GetString("base"), thiscam.GetString("pattern"))
	if err != nil {
		errmsg := fmt.Sprintf("Problem getting latest image: %s", err)
		http.Error(w, errmsg, http.StatusInternalServerError)
		jww.ERROR.Println(errmsg)
		return
	}
	jww.DEBUG.Printf("latest file: %s", file.Name())

	// Do etag check
	etag := fmt.Sprintf("\"%x\"", md5.Sum([]byte(file.ModTime().String())))
	if r.Header.Get("If-None-Match") == etag {
		w.WriteHeader(http.StatusNotModified)
		return
	}

	// new vips transform
	transform := vips.NewTransform().
		StripMetadata().
		LoadFile(filepath.Join(thiscam.GetString("base"), file.Name())).
		Format(imgType)

	if width != "" && height != "" {
		// convert width to int
		widthInt, err := strconv.Atoi(width)
		if err != nil {
			errmsg := fmt.Sprintf("invalid width: %s", err)
			jww.ERROR.Println(errmsg)
			http.Error(w, errmsg, http.StatusBadRequest)
			return
		}

		// convert height to int
		heightInt, err := strconv.Atoi(height)
		if err != nil {
			errmsg := fmt.Sprintf("invalid height: %s", err)
			jww.ERROR.Println(errmsg)
			http.Error(w, errmsg, http.StatusBadRequest)
			return
		}

		// add resize to transform
		transform = transform.Resize(widthInt, heightInt)
	} else if width != "" {
		// convert width to int
		widthInt, err := strconv.Atoi(width)
		if err != nil {
			errmsg := fmt.Sprintf("invalid width: %s", err)
			jww.ERROR.Println(errmsg)
			http.Error(w, errmsg, http.StatusBadRequest)
			return
		}

		// add resize to transform
		transform = transform.ResizeWidth(widthInt)
	} else if height != "" {
		// convert width to int
		heightInt, err := strconv.Atoi(height)
		if err != nil {
			errmsg := fmt.Sprintf("invalid height: %s", err)
			jww.ERROR.Println(errmsg)
			http.Error(w, errmsg, http.StatusBadRequest)
			return
		}

		// add resize to transform
		transform = transform.ResizeHeight(heightInt)
	}

	// handle quality
	if quality != "" {
		qualityInt, err := strconv.Atoi(quality)
		if err != nil {
			errmsg := fmt.Sprintf("invalid quality: %s", err)
			jww.ERROR.Println(errmsg)
			http.Error(w, errmsg, http.StatusBadRequest)
			return
		}

		if qualityInt < 0 {
			jww.WARN.Printf("quality of %d too small. quality = 0\n", qualityInt)
			qualityInt = 0
		}

		if qualityInt > 100 {
			jww.WARN.Printf("quality of %d too small. quality = 100\n", qualityInt)
			qualityInt = 100
		}

		transform = transform.Quality(qualityInt)
	}

	// handle compression
	if compression != "" {
		compressionInt, err := strconv.Atoi(compression)
		if err != nil {
			errmsg := fmt.Sprintf("invalid compression: %s", err)
			jww.ERROR.Println(errmsg)
			http.Error(w, errmsg, http.StatusBadRequest)
			return
		}

		if compressionInt < 0 {
			jww.WARN.Printf("compression of %d too small. compression = 0\n", compressionInt)
			compressionInt = 0
		}

		if compressionInt > 9 {
			jww.WARN.Printf("compression of %d too large. compression = 9\n", compressionInt)
			compressionInt = 9
		}

		transform = transform.Compression(compressionInt)
	}

	// set output for transform
	transform = transform.Output(w)

	// set cache control
	maxage := "0"
	if thiscam.IsSet("max_age") {
		maxage = thiscam.GetString("max_age")
	}
	w.Header().Set("Cache-Control", "max-age="+maxage+", must-revalidate")
	w.Header().Set("ETag", etag)
	w.Header().Set("Last-Modified", file.ModTime().UTC().Format("Mon, 02 Jan 2006 15:04:05")+" GMT")

	// write asset back to client
	w.Header().Set("Content-Type", mtype)
	w.WriteHeader(http.StatusOK)

	// apply transform
	_, _, err = transform.Apply()
	if err != nil {
		jww.WARN.Printf("write error: %s\n", err)
		return
	}
}
