package main

import (
	"fmt"
	"os"
	"path/filepath"

	jww "github.com/spf13/jwalterweatherman"
)

func getLatestImage(path, pattern string) (f os.FileInfo, err error) {
	jww.DEBUG.Printf("getLatestImage: path = %s\n", path)
	jww.DEBUG.Printf("getLatestImage: pattern = %s\n", pattern)

	match := filepath.Join(path, pattern)

	jww.DEBUG.Printf("getLatestImage: match = %s\n", match)
	files, err := filepath.Glob(match)
	if err != nil {
		return nil, err
	}

	// must have two or more files
	if len(files) < 2 {
		return nil, fmt.Errorf("not enough files found (files < 2)")
	}

	f, err = os.Stat(files[len(files) - 2])
	if err != nil {
		return nil, err
	}

	return f, nil
}
