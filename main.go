package main

import (
	"fmt"
	"net/http"
	"runtime"

	"github.com/davidbyttow/govips/pkg/vips"
	"github.com/fsnotify/fsnotify"
	jww "github.com/spf13/jwalterweatherman"
	"github.com/spf13/viper"
	"gitlab.com/andrewheberle/routerswapper"
)

func main() {
	var appName = "go-cam-viewer"

	jww.INFO.Println("starting...")

	// set defaults
	viper.SetDefault("address", defaultAddress)
	viper.SetDefault("port", defaultPort)
	viper.SetDefault("base_path", defaultBasePath)
	viper.SetDefault("log_stdout_level", "error")
	viper.SetDefault("log_level", "info")

	// config setup
	viper.SetConfigName("config")
	viper.AddConfigPath("/etc/" + appName)
	viper.AddConfigPath("$HOME/." + appName)
	viper.AddConfigPath(".")
	err := viper.ReadInConfig()
	if err != nil {
		jww.FATAL.Fatalf("config load error: %s", err)
	}

	// Set stdout threshold
	switch viper.GetString("log_stdout_level") {
	case "fatal":
		jww.SetStdoutThreshold(jww.LevelFatal)
	case "critical":
		jww.SetStdoutThreshold(jww.LevelCritical)
	case "error":
		jww.SetStdoutThreshold(jww.LevelError)
	case "warn":
		jww.SetStdoutThreshold(jww.LevelWarn)
	case "info":
		jww.SetStdoutThreshold(jww.LevelInfo)
	case "debug":
		jww.SetStdoutThreshold(jww.LevelDebug)
	case "trace":
		jww.SetStdoutThreshold(jww.LevelTrace)
	default:
		jww.FATAL.Fatalf("invalid level")
	}

	// Set logging threshold
	switch viper.GetString("log_level") {
	case "fatal":
		jww.SetLogThreshold(jww.LevelFatal)
	case "critical":
		jww.SetLogThreshold(jww.LevelCritical)
	case "error":
		jww.SetLogThreshold(jww.LevelError)
	case "warn":
		jww.SetLogThreshold(jww.LevelWarn)
	case "info":
		jww.SetLogThreshold(jww.LevelInfo)
	case "debug":
		jww.SetLogThreshold(jww.LevelDebug)
	case "trace":
		jww.SetLogThreshold(jww.LevelTrace)
	default:
		jww.FATAL.Fatalf("invalid level")
	}

	// Set up router
	rs := routerswapper.New(newRouter())

	// Watch for config changes
	viper.WatchConfig()
	viper.OnConfigChange(func(e fsnotify.Event) {
		// swap router
		rs.Swap(newRouter())
	})

	// Start up vips and defer vips shutdown
	vipsConfig := new(vips.Config)
	vipsConfig.ConcurrencyLevel = runtime.NumCPU()
	vips.Startup(vipsConfig)
	defer vips.Shutdown()

	if err := http.ListenAndServe(fmt.Sprintf("%s:%d", viper.GetString("address"), viper.GetInt("port")), rs); err != nil {
		jww.FATAL.Fatalf("http error: %s", err)
	}

	jww.INFO.Println("finished")
}
